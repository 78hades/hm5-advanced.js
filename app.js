urlFirst = 'https://ajax.test-danit.com/api/json/users/'
urlSecond = 'https://ajax.test-danit.com/api/json/posts/'

function fetchRequest(url) {
  return fetch(url)
    .then((responseData) => {
      if (!responseData.ok) {
        throw new Error('Error');
      }
      return responseData.json();
    });
}

class User {
  constructor(data) {
    this.id = data.id;
    this.name = data.name;
    this.email = data.email;
  }
}

class Post {
  constructor(data) {
    this.title = data.title;
    this.body = data.body;
    this.userId = data.userId;
    this.postID = data.id
  }
}
const newsLines = document.createElement('main');
class Card {
  constructor(dataUsers, dataPosts) {
    this.users = dataUsers;
    this.posts = dataPosts;

  }

  getInfo() {

    this.posts.forEach(post => {
      const postContainer = document.createElement('div');
      postContainer.classList.add('container');
      const user = this.users.find(user => user.id === post.userId);
      if (user) {
        const userName = document.createElement('span');
        userName.classList.add('user-name');
        userName.textContent = `${user.name}`;
        postContainer.appendChild(userName);
        const userEmail = document.createElement('span');
        userEmail.classList.add('email');
        userEmail.textContent = `${user.email}`;
        postContainer.appendChild(userEmail);
      }

      const userTitle = document.createElement('h3');
      userTitle.classList.add('title');
      userTitle.textContent = `${post.title}`;
      postContainer.appendChild(userTitle);
      const userText = document.createElement('p');
      userText.classList.add('text');
      userText.textContent = `${post.body}`;
      postContainer.appendChild(userText);
      const buttonDelete = document.createElement('button');
      buttonDelete.classList.add(`button`)
      buttonDelete.addEventListener('click', () => {
        const postId = post.postID;
        postDelete(postId, this);
        newsLines.removeChild(postContainer);
      });
      postContainer.appendChild(buttonDelete);
      buttonDelete.innerText = 'Delete post'
      postContainer.appendChild(buttonDelete);
      newsLines.appendChild(postContainer);
    });
    document.body.appendChild(newsLines);
  }
}


Promise.all([fetchRequest(urlFirst), fetchRequest(urlSecond)])
  .then(([dataUsers, dataPosts]) => {
    const users = dataUsers.map(dataUser => new User(dataUser));
    const posts = dataPosts.map(dataPost => new Post(dataPost));

   const cards = new Card(users, posts);
    cards.getInfo();
   
 
  })
  .catch((error) => {
    console.log(error);
  });
  function postDelete(postId, card) {
    console.log(postId);
    fetch(urlSecond + postId, {
      method: 'DELETE',
    })
      .then((response) => response.json())
      .then(() => {
        card.getInfo();
      })
      .catch((error) => {
        console.error(error);
      });
  }